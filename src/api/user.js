import request from '@/api/config.js';

export function getUser(id) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + '_bearer_token' + "=([^;]*)"
      ));
    let token = matches ? decodeURIComponent(matches[1]) : undefined;
    return request({
        url: `/api/users/${id}`,
        headers: {'authorization': token},
        method: 'get'
    })
}

export function getUsers() {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + '_bearer_token' + "=([^;]*)"
      ));
    let token = matches ? decodeURIComponent(matches[1]) : undefined;
    return request({
        url: `/api/users/all`,
        headers: {'authorization': token},
        method: 'get'
    })
}

export function updateUser(id, data) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + '_bearer_token' + "=([^;]*)"
      ));
    let token = matches ? decodeURIComponent(matches[1]) : undefined;
    return request({
        url: `/api/users/${id}`,
        method: 'put',
        headers: {'authorization': token},
        data
    })
}

export function updateUserLike(id, data) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + '_bearer_token' + "=([^;]*)"
      ));
    let token = matches ? decodeURIComponent(matches[1]) : undefined;
    return request({
        url: `/api/users/${id}/addLike`,
        method: 'put',
        headers: {'authorization': token},
        data
    })
}

export function updateUserZoom(id, data) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + '_bearer_token' + "=([^;]*)"
      ));
    let token = matches ? decodeURIComponent(matches[1]) : undefined;
    return request({
        url: `/api/users/${id}/addZoom`,
        method: 'put',
        headers: {'authorization': token},
        data
    })
}