import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//css
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/css/util.css'
import '@/css/main.css'

//fonts
import '@fortawesome/fontawesome-free/css/fontawesome.min.css'
import '@fortawesome/fontawesome-free/css/solid.min.css'
import '@fortawesome/fontawesome-free/css/regular.min.css'
import '@fortawesome/fontawesome-free/css/brands.min.css'

//js
import 'bootstrap'


import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import BootstrapVue from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)

Vue.use(ElementUI)

import VueSocketIO from 'vue-socket.io';

Vue.use(new VueSocketIO({
    debug: process.env.NODE_ENV !== 'production',
    connection: 'http://localhost:3000'
  })
);
// Vue.use(VueSocketIO, 'http://localhost:3000')

import { VueMaskDirective } from 'v-mask'
Vue.directive('mask', VueMaskDirective);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
