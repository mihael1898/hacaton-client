import { getUser } from '../../api/user'
// initial state
const state = {
  user: null
}

// getters
const getters = {}

// actions
const actions = {
  getDataUser ({ commit }) {
    return new Promise(resolve =>
      // getUser('+79500996585').then(user => {
      //   user.data.Orders.sort((a,b) => { 
      //     if (a.createdAt > b.createdAt) {
      //       return -1
      //     }
      //     return 1
      //   })
      //   commit('setUser', user.data)
      //   resolve(user.data)
      // })
      {
        var name = 'session'
        let matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name + "=([^;]*)"
        ));
        var _user = matches ? decodeURIComponent(matches[1]) : undefined;
        if (_user) {
          _user = JSON.parse(window.atob(_user));
          getUser(_user.passport.user).then(user => {
            commit('setUser', user.data)
            resolve(user.data)
          })
        }
        else {
          name = '_user'
          matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name + "=([^;]*)"
          ));
          _user = matches ? decodeURIComponent(matches[1]) : undefined;
          if (_user) {
            getUser(_user).then(user => {
              commit('setUser', user.data)
              resolve(user.data)
            })
          }
          else {
            commit('setUser', null)
            resolve(null)
          }
          
        }
        
      }
      
    )
  }
}

// mutations
const mutations = {
  setUser (state, user) {
    state.user = user
  },
  setName (state, name) {
    state.user.name = name
  },
  setSurname (state, surname) {
    state.user.surname = surname
  },
  setEmail (state, email) {
    state.user.email = email
  },
  setAbout (state, about) {
    state.user.about = about
  },
  setPosition (state, position) {
    state.user.position = position
  },
  setImage (state, url_img) {
    state.user.url_img = url_img
  },
  addLike (state, user) {
    state.user.children.push(user)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
