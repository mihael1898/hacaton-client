import request from '@/api/config.js';

export function loginUser(data) {
    return request({
        url: `/api/login`,
        method: 'post',
        data
    })
}

export function getSms(data) {
    return request({
        url: `/api/register`,
        method: 'post',
        data
    })
}

export function getPassword(data) {
    return request({
        url: `/api/password`,
        method: 'post',
        data
    })
}