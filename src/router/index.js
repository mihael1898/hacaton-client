import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/login.vue'
// import ModalProduct from '../components/ModalProduct.vue'
import 'nprogress/nprogress.css'
import NProgress from 'nprogress'
import store from '../store/index.js'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    // children: [
    //   { path: 'product/:id', component: ModalProduct },
    // ]
  },
  {
    path: '/',
    name: 'Home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/home.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/about.vue')
  },
  // {
  //   path: '/promotions',
  //   name: 'Promotions',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/promotions.vue')
  // },
  // {
  //   path: '/order',
  //   name: 'Order',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/shoping-cart.vue')
  // },
  {
    path: '/profile',
    name: 'Profile',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/profile/index.vue'),
    meta: { 
      user: true
    }
  },
  // {
  //   path: '/admin',
  //   name: 'Admin',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/admin/index.vue'),
  //   meta: { 
  //     admin: true
  //   }
  // },
  // {
  //   path: '/manager',
  //   name: 'Manager',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/manager/index.vue'),
  //   meta: { 
  //     manager: true
  //   }
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.user)) {
    if (store.state.user.user) {
        next()
        return
    }
    next('/')
  }
  // } else if (to.matched.some(record => record.meta.admin)) {
  //   if (store.state.user.user && store.state.user.user.user_role === 2) {
  //       next()
  //       return
  //   }
  //   next('/')
  // } else if (to.matched.some(record => record.meta.manager)) {
  //   if (store.state.user.user && store.state.user.user.user_role > 1) {
  //       next()
  //       return
  //   }
  //   next('/')
  // }
  next()
})

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    document.documentElement.scrollTop = 0;
    NProgress.start()
  }
  next()
})

router.afterEach(() => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router
